# Changelog

Changes to the this VSC extension will be documented in this file.

## Version 1.2.2

- Add fatscript.repl command and menu (repl icon)

## Version 1.2.1

- Fix minor issues on the package.json definitions

## Version 1.2.0

- Add fatscript.format command and menu (sparkle icon)
- Changes the fatscript.fun menu icon to flame
- Removes backwards compatibility (requires fry >1.2.0)

## Version 1.1.3

- Show as "Run (filename)" on the terminal tab
- Add fatscript.run button to editor/title bar

## Version 1.1.2

- Add fatscript.run command

## Version 1.1.1

- Revert logo weird effect

## Version 1.1.0

- Use stdout as result if possible
- Add glow effect to logo

## Version 1.0.1

- Update logo to match new branding

## Version 1.0.0

- Initial release
