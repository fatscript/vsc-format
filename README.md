# FatScript Formatter (VSC Extension)

This project provides a formatter and a code runner for the FatScript programming language in Visual Studio Code, using the [fry](https://gitlab.com/fatscript/fry) interpreter. For more information about FatScript language specification and syntax, please visit:

- [fatscript.org](https://fatscript.org) (official docs)

## Installation

> [fry](https://gitlab.com/fatscript/fry) needs to be installed on your system for this extensions to work

1. Install Visual Studio Code if you haven't already.
2. Open the extensions panel.
3. Search for "FatScript Formatter".
4. Click the "Install" button for the extension.

Alternatively, you can install the extension from source:

1. Clone or download the repository to your local machine.
2. Install the [Visual Studio Code Extension SDK](https://code.visualstudio.com/docs/extensions/example-hello-world#_step-1-set-up-your-development-environment).
3. Run `vsce package` to create the VSIX package for the extension.
4. In Visual Studio Code, click on the "..." icon in the extensions panel and select "Install from VSIX".

## Usage

### Formatting scripts

Once the FatScript formatter extension is installed, it will format FatScript code files (.fat) using `fry`. You can format a file by selecting "Format Document" from the editor context menu or by using the keyboard shortcut.

### Running scripts

In addition to formatting FatScript files, this extension also contributes a "Run FatScript" command, which will run your current FatScript file in a new terminal window using `fry`.

To execute this command, open the Command Palette (`Ctrl+Shift+P` on Windows/Linux or `Cmd+Shift+P` on MacOS) and type "Run FatScript". Select the command from the dropdown to run your current file.

For quicker access, you can assign a keyboard shortcut to this command via `Keyboard Shortcuts` settings, choosing any key combination you prefer to easily run your code.

## Contributing

If you find any issues with this formatter extension or would like to contribute to the project, please [create an issue](https://gitlab.com/fatscript/vsc-format/-/issues) or merge request on the project's [GitLab repository](https://gitlab.com/fatscript/vsc-format).

### Donations

Did you find this VSC extension useful and would like to say thanks?

[Buy me a coffee](https://www.buymeacoffee.com/aprates)

## License

[GPLv3](LICENSE) © 2023 Antonio Prates
