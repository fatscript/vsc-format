const vscode = require('vscode');
const { exec } = require('child_process');
const path = require('path');

function activate(context) {
  const formatter = vscode.languages.registerDocumentFormattingEditProvider('fatscript', {
    provideDocumentFormattingEdits(document) {
      return new Promise((resolve, reject) => {
        document.save().then(() => {
          const command = `fry -p -f "${document.fileName}"`;

          exec(command, (error, stdout, stderr) => {
            if (error) {
              vscode.window.showErrorMessage(`Error formatting FatScript: ${stderr}`);
              reject(error);
            } else {
              const fullRange = new vscode.Range(
                document.positionAt(0),
                document.positionAt(document.getText().length)
              );

              resolve([vscode.TextEdit.replace(fullRange, stdout)]);
            }
          });
        });
      });
    },
  });

  context.subscriptions.push(formatter);

  const formatCommand = vscode.commands.registerCommand('fatscript.format', () => {
    const editor = vscode.window.activeTextEditor;
    if (!editor) return;

    const document = editor.document;
    if (document.languageId !== 'fatscript') return;

    vscode.commands.executeCommand('editor.action.formatDocument');
  });

  context.subscriptions.push(formatCommand);

  const runCommand = vscode.commands.registerCommand('fatscript.run', () => {
    const editor = vscode.window.activeTextEditor;
    if (!editor) return;

    const document = editor.document;
    const terminal = vscode.window.createTerminal(`Run ${path.basename(document.fileName)}`);

    document.save().then(() => {
      terminal.sendText(`fry "${document.fileName}"`);
      terminal.show();
    });
  });

  context.subscriptions.push(runCommand);

  const replCommand = vscode.commands.registerCommand('fatscript.repl', () => {
    const editor = vscode.window.activeTextEditor;
    if (!editor) return;

    const document = editor.document;
    const terminal = vscode.window.createTerminal(`REPL ${path.basename(document.fileName)}`);

    document.save().then(() => {
      terminal.sendText(`fry -i -s "${document.fileName}"`);
      terminal.show();
    });
  });

  context.subscriptions.push(replCommand);
}

exports.activate = activate;
